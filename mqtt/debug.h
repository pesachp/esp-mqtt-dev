/*

 * debug.h
 *
 *  Created on: Dec 4, 2014
 *      Author: Minh
 */


extern int enable_debug_messages; // Needed by debug.h

#ifndef USER_DEBUG_H_
	#define USER_DEBUG_H_

	#define DEBUG 1
	#define INFO 2
	#define RESPONSE 4

#endif /* USER_DEBUG_H_ */

